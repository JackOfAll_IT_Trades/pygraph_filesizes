
import os
import os.path as path

import matplotlib.pyplot as plt

import argparse


File_extensions_to_analyze = [".jpeg", ".jpg", ".mov"]


DEBUG_PRINT_EACH_DIRECTORY_SCANNED   = True  # False
DEBUG_PRINT_FILE_SIZE_PERCENTAGE_MAP = True  # False


def build_file_size_list(search_path, extensions_to_analyze) :
	file_size_list = []

	for dirpath, dirs, files in os.walk(search_path, topdown=True) :
		if DEBUG_PRINT_EACH_DIRECTORY_SCANNED:
			search_path_length = len(search_path)
			abbreviated_dirpath = dirpath[search_path_length+1:]
			print( "PyGraph_FileSizes -- Scanning directory <{0}>...".format(abbreviated_dirpath) )
		#
		# END if DEBUG_PRINT_EACH_DIRECTORY_SCANNED: ...

		for file_name in files:
			#
			# Check to see if our file has one of the extensions in the list:
			#
			extension_matches = False
			for file_extension in extensions_to_analyze:
				if file_name.endswith(file_extension) :
					extension_matches = True
					break
				#
				# END if file_name.endswith() : ...
			#
			# END for file_extension in extensions_to_analyze: ...

			# Skip files whose extension is not in our list:
			#
			if not extension_matches: continue

			file_path = path.join(dirpath, file_name)
			file_size = path.getsize(file_path)

			file_size_list = file_size_list + [file_size]
		#
		# END for file_name in files: ...
	#
	# END for dirpath, dirs, files in os.walk(search_path, topdown=True) : ...

	return file_size_list
#
# END build_file_size_list(search_path, extensions_to_analyze) : ...


def build_file_size_percentage_map_list(file_size_list) :
	#
	# Analyze our list of file sizes and figure out what percentage
	# of files are less than or equal to each one in the list:
	#
	size_list_length = len(file_size_list)
	sorted_size_list = sorted(file_size_list)

	size_percentage_map_list  = [ (0, 0) for file_size in sorted_size_list ]
	size_percentage_map_list += [ (0, 0) ]

	percentage_for_each_file    = float(100) / float(size_list_length)
	percentage_log_msg_template = "PyGraph_FileSizes -- Computed percentage_for_each_file -- with [{0}] files, each file is [{1}]% of the total set."
	percentage_log_message_str  = percentage_log_msg_template.format(size_list_length, percentage_for_each_file)
	print(percentage_log_message_str)

	for index, current_file_size in enumerate(sorted_size_list, start=1) :
		current_percentage = index * percentage_for_each_file
		size_percentage_map_list[index] = (current_file_size, current_percentage)
	#
	# END for index, file_size in enumerate(sorted_size_list, start=1) : ...

	if DEBUG_PRINT_FILE_SIZE_PERCENTAGE_MAP:
		print( "PyGraph_FileSizes -- size_percentage_map_list = [" )

		for (file_size, percentage_less_or_equal) in size_percentage_map_list:
			print( "PyGraph_FileSizes -- \t\t( {0}, {1} )".format(file_size, percentage_less_or_equal) )
		#
		# END for (file_size, percentage_less_or_equal) in size_percentage_map_list: ...

		print( "PyGraph_FileSizes -- ]" )
	#
	# END if DEBUG_PRINT_FILE_SIZE_PERCENTAGE_MAP: ...

	return size_percentage_map_list
#
# END build_file_size_percentage_map_list(file_size_list) : ...


def show_file_size_percentage_scatter_plot(size_percentage_map_list, analyzed_path) :
	(size_list, percentage_list) = list(zip(*size_percentage_map_list))

	plt.scatter      ( size_list, percentage_list )
	plt.plot         ( size_list, percentage_list )
	plt.fill_between ( size_list, percentage_list )

	plt.xlabel  ( "File size (bytes)" )
	plt.ylabel  ( "% of files under that size" )

	plt.title   ( "File size distribution in\n<{0}>".format(analyzed_path) )

	plt.tight_layout()

	plt.show()

	return
#
# END show_file_size_percentage_scatter_plot() : ...



if __name__ == "__main__":

	print("PyGraph_FileSizes -- Starting up...")
	print("PyGraph_FileSizes --")

	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument("-p", "--search_path", type=str, default=".")

	args = arg_parser.parse_args()


	print("PyGraph_FileSizes -- Will search directory: <{0}>...".format(args.search_path) )
	print("PyGraph_FileSizes -- ...to get sizes of [{0}] files.".format(File_extensions_to_analyze) )
	print("PyGraph_FileSizes --")


	print("PyGraph_FileSizes -- Building file size list...")

	file_size_list = build_file_size_list( args.search_path,
										   File_extensions_to_analyze )

	print("PyGraph_FileSizes -- Found [{0}] files with requested extensions.".format(len(file_size_list)) )


	print("PyGraph_FileSizes --")
	print("PyGraph_FileSizes -- Building size percentage map...".format(len(file_size_list)) )

	size_percentage_map_list = build_file_size_percentage_map_list( file_size_list )

	print("PyGraph_FileSizes -- Made file size percentage map with [{0}] elements...".format(len(size_percentage_map_list)) )
	print("PyGraph_FileSizes --")

	print("PyGraph_FileSizes -- Preparing file size percentage graph to show...")

	show_file_size_percentage_scatter_plot(size_percentage_map_list, args.search_path)

	print("PyGraph_FileSizes --")
	print("PyGraph_FileSizes -- All done! :)")
	print("PyGraph_FileSizes --")
#
# END if __name__ == "__main__": ...

